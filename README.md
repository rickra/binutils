Some utils I use for shortcuts.
install by cloning into some `$PATH` directory.

# shoot
screen recording in all forms

## usage
```sh
shoot video
shoot video active
shoot screen
shoot screen select
shoot screen active
```

# containermenu
starts, stops and launches shells in containers with bemenu
## usage
```sh
containermenu start
containermenu stop
containermenu shell
```

# gopassmenu
extract entries from a gopass entry with a demu-like of your choice

## usage
`gopassmenu <key> [<key> ...]`

To replicate the standard pattern of typing username and password call:
```sh
gopassmenu username {TAB} "" {ENTER}
```
note the empty string for the secret, see [gopass show documentation](https://github.com/gopasspw/gopass/blob/master/docs/commands/show.md) for key usage, this example expects a secret layout like:
```
correcthorsebatterystaple
username: examplename
```

To only type the password of a selected entry call:
```sh
gopassmenu ""
```

## configuration
Configuration is done via environment variables:
- `GOPASSMENU_DMENU` the dmenu command to run (take all options in stdin), default: `bemenu`
- `GOPASSMENU_TYPE` the command to do the typing (take type sequence in stdin), default: `ydotool type --file /dev/stdin`
